#!/bin/bash
#   _       _     
#  | |     | |    
#  | |  ___| |__  
#  | | / __| '_ \ 
#  | |_\__ \ | | |
#  |_(_)___/_| |_|
#
if [ -d "${1}" ]; then # -------------------------------------------- IS DIR
  if [ "${2}" == "to" ]; then # ------------------------------------- SEND LIST
    if [[ "${3}" == *\/* ]]; then # --------------------------------- SEND PER SCP
      scp -r "${1}" "${3}"
    else # ---------------------------------------------------------- SEND PER MAIL
      ls -lA $1 | mail -s "Verzeichnis: ${1}" "${3}"
    fi
  else #------------------------------------------------------------- LIST DIR 
    ls -lA $1
  fi
else
  if [ -f "${1}" ]; then # ------------------------------------------ IS FILE
      if file -N $1 | cut -d: -f2 | grep "text" >/dev/null; then # -- CONTAINS TEXT
        if [ "${2}" == "to" ]; then # ------------------------------- SEND CONTENT
          cat $1 | mail -s "Datei: ${1}" "${3}"
        else # ------------------------------------------------------ SHOW CONTENT
          less $1
        fi
      else # -------------------------------------------------------- NO TEXT CONTENT
        echo -e "\e[01;31mFehler:\e[00m '${1}' enthält keinen Text!"
        echo -en '\e[00m'
      fi
  else # ------------------------------------------------------------ FUUU!?
    if [ "${1}" == "--help" ]; then # ------------------------------- HELP
      echo -e "\e[01;32mHilfe:\e[00m" 
      echo -e "\e[01;30m"
      echo "        _       _     "
      echo "       | |     | |    "
      echo "       | |  ___| |__  "
      echo "       | | / __| '_ \ "
      echo "       | |_\__ \ | | |"
      echo "       |_(_)___/_| |_|"
      echo -e '\e[00m'
      echo " Verzeichnisinhalt lesen .........: l /path/to/dir"
      echo " Verzeichnisinhalt per Mail ......: l /path/to/dir to user@host.tld"
      echo " Verzeichnisinhalt -R per scp ....: l /path/to/dir to user@host.tld:/path/to/drop"
      echo " Funktion als Parameter übergeben.: l \$(cat /file/with/dirname) to user@host.tld"
      echo " Dateiinhalt .....................: l /path/to/file.ext"
      echo " Dateiinhalt per Mail ............: l /path/to/file.ext to user@host.tld"
      echo " Diese Hile anzeigen .............: l --help"
      echo " l.sh als Alias 'l' einrichten ...: l --install"
      echo
    else
      if [ "${1}" == "--install" ]; then # -------------------------- INSTALL
        echo "alias l='${0}'" >> ~/.bashrc
        source ~/.bashrc
      else # -------------------------------------------------------- DEFAULT LIST DIR
        ls -lA $(pwd)
      fi
    fi
  fi
fi
